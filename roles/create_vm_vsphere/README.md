create_vm_vsphere
=========

This role creates a VM from a template in VSphere.
This role will also check the infoblox server and get available ip address and assign it to the freshly created VM. It will also get a valid DNS name for the VM.

Requirements
------------

We require a VSphere template to run this role successfully.

