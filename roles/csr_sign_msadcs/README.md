csr_sign_msadcs
=========

This role will generate a CSR and it will sign that CSR with the Microsoft ADCS and get the signed certificate back.

Requirements
------------

We need to have a valid Microsoft Active Directory Certificate Services account and update the nois_provider variable in the all_vars folder 

