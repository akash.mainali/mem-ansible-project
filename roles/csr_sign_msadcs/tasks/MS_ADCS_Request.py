 
import sys
from certsrv import Certsrv

cert_server = "mem-co-ca1.mem-ins.com" 
cert_template = "WebServerV2"

def get_key(username, password, csr_path):
    # Read existing CSR
    signing_req = open(csr_path, 'r').read().encode('utf-8')
   
    # Get the cert from the ADCS server
    ca_server = Certsrv(cert_server, username, password, auth_method='ntlm')

    pem_cert = ca_server.get_cert(signing_req, cert_template)

    with open('ca_signed.cert', 'w') as cert: cert.write(pem_cert.decode())

if __name__ == "__main__":
   username = sys.argv[1]
   password = sys.argv[2]
   csr_path = sys.argv[3] get_key(username, password, csr_path)
