tomcat_config
=========

This role configures the tomcat server which was presenet on the template responsible to create the VM.
This roles configures the tomcat server according to the requirement of MEM.

Requirements
------------

We require a template inside a role to create a setenv.sh file and tomcat config file.
